package com.example.showtime

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import java.util.*

class MainActivity : AppCompatActivity() {
    private lateinit var locationManager: LocationManager
    private lateinit var locationListener: LocationListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val mTime = IntentFilter(Intent.ACTION_TIME_TICK)
        registerReceiver(myBroadcastReceiver, mTime)

        val latitudeTextView = findViewById<TextView>(R.id.latitudeTextView)
        val longitudeTextView = findViewById<TextView>(R.id.longitudeTextView)

        locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                val latitudeCoordinate = location.latitude
                val longitudeCoordinate = location.longitude

                latitudeTextView.text = "Latitude: $latitudeCoordinate"
                longitudeTextView.text = "Longitude: $longitudeCoordinate"

            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
                // Handle status changes here
            }

            override fun onProviderEnabled(provider: String) {
                // Handle provider enabled here
            }

            override fun onProviderDisabled(provider: String) {
                // Handle provider disabled here
            }
        }

        locationManager =
            this@MainActivity.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (!this.shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)
        ) {
            requestPermissionLauncherInLocation.launch(Manifest.permission.ACCESS_FINE_LOCATION)
        } else {
            settingsIntent()
        }
    }

    private val myBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(c: Context, i: Intent) {
            val showTimeTextView = findViewById<TextView>(R.id.showTimeTextView)
            val time = Calendar.getInstance()
            Toast.makeText(
                applicationContext,
                "current time is--- : -" + time.time,
                Toast.LENGTH_LONG
            ).show()
            showTimeTextView.text = "" + time.time
        }
    }


    private val requestPermissionLauncherInLocation =
        registerForActivityResult(
            ActivityResultContracts.RequestPermission()
        ) { isGranted: Boolean ->
            if (isGranted) {
                showCurrentLocation()
            } else {
                settingsIntent()
            }
        }

    private fun showCurrentLocation() {
        if (ActivityCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this@MainActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                0,
                0F,
                locationListener
            )
        }
    }

    private fun requestPermissionLocation(grantResults: IntArray) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    0,
                    0F,
                    locationListener
                )
            }
        }
//        else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
//            settingsIntent()
//        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        requestPermissionLocation(grantResults)
    }

    private val startResultActivity = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {}

    private fun settingsIntent() {
        val openSettingsIntent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + this@MainActivity.packageName)
        )
        startResultActivity.launch(openSettingsIntent)
    }
}
